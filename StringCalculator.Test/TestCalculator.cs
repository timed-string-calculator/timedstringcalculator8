﻿using NUnit.Framework;
using System;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestCalculator
    {
        private Calculator _calculator;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,2,7");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewlineAsDelmiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,2\n7");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;2;7");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumbers_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative not allowed -7, -9";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;2;-7;-9"));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumberMoreThanThousand_WHEN_Adding_THEN_IgnoreThem()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;2;7;10002");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;;\n1;;2;;7");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterMultipleCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[*][.]\n1*2.7");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterMultipleCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[**][..]\n1**2..7");

            Assert.AreEqual(expected, actual);
        }
    }
}
