﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customDelimiterId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numbersSection = GetNumbersSection(numbers);
            string[] delimiters = GetDelimiters(numbers);
            List<int> numbersList = GetNumbers(numbersSection, delimiters);

            ValidateNumbers(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var numbers in numbersList)
            {
                if (numbers < 0)
                {
                    negativeNumbers.Add(numbers.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(", ", negativeNumbers)}");
            }
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimitersId = $"{customDelimiterId}[";
            var multipleCustomSeperator = $"]{newline}";
            var multipleCustomSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimitersId))
            {
                string delimiters = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomSeperator) - (multipleCustomSeperator.Length + 1));

                return delimiters.Split(new[] { multipleCustomSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", "\n" };
        }

        private string GetNumbersSection(string numbers)
        {
            if (numbers.StartsWith(customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string numbers, string[] delimiters)
        {
            List<int> numbersList = new List<int>();
            string[] numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
